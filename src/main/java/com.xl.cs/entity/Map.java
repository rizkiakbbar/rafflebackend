package com.xl.cs.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "map")
public class Map {

	@Id
	@Column(name = "keyMap", nullable = false, columnDefinition="VARCHAR(64)")
	private String keyMap;

	@Column(name = "valueMap")
	private String valueMap;

	public String getKeyMap() {
		return keyMap;
	}

	public void setKeyMap(String keyMap) {
		this.keyMap = keyMap;
	}

	public String getValueMap() {
		return valueMap;
	}

	public void setValueMap(String valueMap) {
		this.valueMap = valueMap;
	}
}
