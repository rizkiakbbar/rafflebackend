package com.xl.cs.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class MyJsonMapper extends ObjectMapper {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3565333494236259513L;

	public MyJsonMapper() {
		this.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
	}
}