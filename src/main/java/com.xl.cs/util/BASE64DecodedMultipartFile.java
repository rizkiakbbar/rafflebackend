package com.xl.cs.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.web.multipart.MultipartFile;

public class BASE64DecodedMultipartFile implements MultipartFile {
	/**
	 * 
	 */

	private final byte[] fileContent;
	private String filename;

	public BASE64DecodedMultipartFile(byte[] fileContent, String filename) {
		this.fileContent = fileContent;
		this.filename = filename;
	}

	public String getName() {
		// TODO - implementation depends on your requirements
		return filename;
	}

	public String getOriginalFilename() {
		// TODO - implementation depends on your requirements
		return filename;
	}

	public String getContentType() {
		// TODO - implementation depends on your requirements
		return null;
	}

	public boolean isEmpty() {
		return fileContent == null || fileContent.length == 0;
	}

	public long getSize() {
		return fileContent.length;
	}

	public byte[] getBytes() throws IOException {
		return fileContent;
	}

	public InputStream getInputStream() throws IOException {
		return new ByteArrayInputStream(fileContent);
	}

	public void transferTo(File dest) throws IOException, IllegalStateException {
		FileOutputStream os = new FileOutputStream(dest);
		try {
			os.write(fileContent);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (os != null) {
				os.close();
			}
		}
	}

}
