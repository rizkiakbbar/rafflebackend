package com.xl.cs.util;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;

import com.xl.cs.constant.AppsConstant;

public class CommonUtil {

	public static Date getEndOfDay(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		return calendar.getTime();
	}

	public static Date getStartOfDay(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}
	
	public static String dataFormating(String str) {
		DecimalFormat df = new DecimalFormat("#.#");
		df.setRoundingMode(RoundingMode.CEILING);
		String data = "";
		if(str.length()<=3) {
			data = str+" "+AppsConstant.BYTE;
		}else if(str.length()>3 && str.length()<=6) {
			double d = Double.parseDouble(str)/1024;
			data = df.format(d)+" "+AppsConstant.KB;
		}else if(str.length()>6 && str.length()<=9) {
			double d = (Double.parseDouble(str)/1024)/1024;
			data = df.format(d)+" "+AppsConstant.MB;
		}else {
			double d = ((Double.parseDouble(str)/1024)/1024)/1024;
			data = df.format(d)+" "+AppsConstant.GB;
		}
		return data;
	}
	
	public static String dataFormatingMB(String str) {
		DecimalFormat df = new DecimalFormat("#.#");
		df.setRoundingMode(RoundingMode.CEILING);
		String data = "";
		if(str.length()<=3) {
			data = str+" "+AppsConstant.BYTE;
		}else if(str.length()>3 && str.length()<=6) {
			double d = Double.parseDouble(str)/1024;
			data = df.format(d)+" "+AppsConstant.KB;
		}else {
			double d = (Double.parseDouble(str)/1024)/1024;
			data = df.format(d)+" "+AppsConstant.MB;
		}
		return data;
	}
	
	public static String dataFormatingKB(String str) {
		DecimalFormat df = new DecimalFormat("#.#");
		df.setRoundingMode(RoundingMode.CEILING);
		String data = "";
		if(str.length()<=3) {
			data = str+" "+AppsConstant.BYTE;
		}else {
			double d = Double.parseDouble(str)/1024;
			data = df.format(d)+" "+AppsConstant.KB;
		}
		return data;
	}
	
	public static String smsFormating(String str) {
		DecimalFormat df = new DecimalFormat("#.#");
		df.setRoundingMode(RoundingMode.CEILING);
		double d = Double.parseDouble(str);
		return df.format(d)+" "+AppsConstant.SMS;
	}
	
	public static String voiceFormating(String str) {
		String data = "";
//		int jam = Integer.valueOf(str) / 3600;
//		int menit = (Integer.valueOf(str) - (jam*3600))/60;
//		int detik = (Integer.valueOf(str) - (jam*3600)) % 60;
//		if(jam !=0) data+= jam + " "+AppsConstant.JAM+" ";
//		if(menit !=0) data+= menit + " "+AppsConstant.MENIT+" ";
//		if(detik !=0) data+= detik + " "+AppsConstant.DETIK;
		data = str+" "+AppsConstant.DETIK;
		return data;
	}
}
