package com.xl.cs.dao;

import java.util.List;

import com.xl.cs.entity.Candidate;

public interface CandidateDao extends GenericDao<Candidate, Long> {
	
//	List<Candidate> getCandidate1();
//	
//	List<Candidate> getCandidate15();
//	
//	List<Candidate> getCandidate50();
//	
//	List<Candidate> getWinner50();
//	
//	List<Candidate> getWinner15();
//	
//	List<Candidate> getWinner1();
	
	List<Candidate> getCandidateByStatus(String status);
	
	List<Candidate> getCandidateByStatus(String status,String batch);
	
	Candidate getCandidateByCoupon(String coupoun);
	
	List<Candidate> getShuffledCandidate();
	
	List<Candidate> getShuffledCandidate(String batch);

}
