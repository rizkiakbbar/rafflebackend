package com.xl.cs.controller;

import java.util.List;

import freemarker.log.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xl.cs.api.request.UpdateRequest;
import com.xl.cs.api.response.UpdateResponse;
import com.xl.cs.bean.CountParticipant;
import com.xl.cs.bean.Subscribers;
import com.xl.cs.entity.Candidate;
import com.xl.cs.entity.Map;
import com.xl.cs.service.CandidateService;
import com.xl.cs.service.MapService;

@RequestMapping("/api")
@RestController
public class SOAController {

	Logger logger = Logger.getLogger(String.valueOf(SOAController.class));

	@Autowired
	CandidateService candidateService;

	@Autowired
	MapService mapService;
	
	@Value("${total.winner}")
	private String totalWinner;
	
	@Value("${total.winner2}")
	private String totalWinner2;
	
	@Value("${total.winner3}")
	private String totalWinner3;

	@RequestMapping(value = "/getParticipantByType", method = RequestMethod.GET, produces = "application/json")
	Subscribers getParticipant(@RequestParam(value = "status", required = true) String status) {
		// TODO randomly sorted
		Subscribers s = new Subscribers();
		s.setSubscribers(candidateService.getCandidateByStatus(status));
		return s;
	}

	@RequestMapping(value = "/getParticipantBatchByType", method = RequestMethod.GET, produces = "application/json")
	Subscribers getParticipantBatchByType(@RequestParam(value = "status", required = true) String status) {
		// TODO randomly sorted
		Map curBatch = mapService.get("batch");
		Subscribers s = new Subscribers();
		s.setSubscribers(candidateService.getCandidateByStatus(status, curBatch.getValueMap()));
		return s;
	}

	@RequestMapping(value = "/getParticipant", method = RequestMethod.GET, produces = "application/json")
	Subscribers getParticipant() {
		Subscribers s = new Subscribers();
		s.setSubscribers(candidateService.getShuffledCandidate());
		return s;
	}
	
	@RequestMapping(value = "/getCountParticipant", method = RequestMethod.GET, produces = "application/json")
	CountParticipant getCountParticipant() {
		CountParticipant s = new CountParticipant();
		s.setTotalParticipant(Long.valueOf(candidateService.getAll().size()));
		return s;
	}

	@RequestMapping(value = "/getParticipantBatch", method = RequestMethod.GET, produces = "application/json")
	Subscribers getParticipantBatch() {
		Subscribers s = new Subscribers();
		Map curBatch = mapService.get("batch");
		List<Candidate> lCand = candidateService.getCandidateByStatus("AVAILABLE",curBatch.getValueMap());
		if(lCand.size() > 0) {
			List<Candidate> lCand1 = candidateService.getCandidateByStatus("WINNER",curBatch.getValueMap());
			List<Candidate> lCand2 = candidateService.getCandidateByStatus("CANCEL",curBatch.getValueMap());
			lCand1.addAll(lCand);
			lCand1.addAll(lCand2);
			s.setSubscribers(lCand1);
		}else {
			curBatch.setValueMap(String.valueOf(Integer.valueOf(curBatch.getValueMap()) + 1));
			curBatch = mapService.save(curBatch);
			s.setSubscribers(candidateService.getShuffledCandidate(curBatch.getValueMap()));
		}
		return s;
	}

	@RequestMapping(value = "/raffleUpdate", method = RequestMethod.POST, produces = "application/json")
	UpdateResponse raffleUpdate(@RequestBody UpdateRequest request) {
		Candidate candidate = candidateService.get(Long.valueOf(request.getId()));
		candidate.setNote(request.getNote());
		if (request.getStatus().contains("WINNER")) {
			List<Candidate> lCand = candidateService.getCandidateByStatus("WINNER");
			if (lCand.size() >= 0 && lCand.size() < Long.valueOf(totalWinner3) ) {
				candidate.setStatus("WINNERUANG");
			} else if (lCand.size() >= Long.valueOf(totalWinner3) && lCand.size() < Long.valueOf(totalWinner2)) {
				candidate.setStatus("WINNEREMAS");
			} else if (lCand.size() >= Long.valueOf(totalWinner2)) {
				candidate.setStatus("WINNERMOTOR");
			}
		} else {
			candidate.setStatus(request.getStatus());
		}
		candidateService.save(candidate);
		UpdateResponse resp = new UpdateResponse();
		resp.setCode("200");
		resp.setType(request.getStatus());
		return resp;
	}
}
