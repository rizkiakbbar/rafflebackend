package com.xl.cs.constant;

public class AppsConstant {
	public static final String LDAP_SUCCESS = "00";
	public static final String LDAP_FAILED = "01";
	public static final String LDAP_CON_FAILED = "02";
	public static final String LDAP_UNKNOWN = "99";
	public static final String TOKEN_HASH = "cb057160-e08e-41f8-b316-fb65368c250d";
	public static final String TOKEN_LOGIN = "426d73a9-dec4-4735-85b6-c0b294fa73a8";
	public static final String TOKEN_REGISTER = "b50fa65f-f304-40e1-b7cb-fcb4e99934f1";
	public static final String SUPER_ADMIN_ID = "1";
	public static final String DETIK = "DETIK";
	public static final String SMS = "SMS";
	public static final String GB = "GB";
	public static final String MB = "MB";
	public static final String KB = "KB";
	public static final String BYTE = "BYTE";
	public static final String PRICEPLAN = "PricePlan";
	public static final String AO = "Additional Offer";
	public static final String NONPERSISTED = "nonpersisted";
	public static final String CONSTANT = "constant";
	public static final int EXPIRED_DAYS = 4;
	public static final String ACTIVE = "Active";
	public static final String NOT_ACTIVE = "Not Active";
	public static final String CANCEL = "Cancel";
	public static final String BALANCE_TYPE = "3";
	public static final String BALANCE_DOMPUL_TYPE = "4";
	public static final String RUPIAH = "Rupiah";
	public static final String DOMPUL = "DomPul";
	public static final String YES = "Yes";
	public static final String NO = "No";
	public static final String ERROR = "Err";
	public static final String ACTIVE_SUBSCRIBER_STATUS = "A";
	public static final String[] PRICEPLAN_GARUDA = { "470768", "464728", "440358", "513707854", "513780784", "5138921",
			"513786964", "513722974" };
	public static final int CH_ID_MSISDN = 100;
	public static final int CH_ID_VALID_MSISDN = 101;
	public static final int CH_ID_IR_SUCCESS = 200;
	public static final int CH_ID_IS_ACTIVE = 300;
	public static final int CH_ID_IS_PP_GARUDA = 400;
	public static final int CH_ID_IS_SPWL = 500;
	public static final int CH_ID_IS_RELOAD = 600;
	public static final int CH_ID_ACTIVE_SINCE = 700;
	public static final int CH_ID_ALLOWANCE_EXIST = 800;
	public static final int CH_ID_ALLOWANCE_USED = 900;
	public static final int CH_ID_ALLOWANCE_REMAINING = 1000;
	public static final int CH_ID_PENDING_QUEUE = 1100;
	public static final int CH_ID_LAST_RELOAD_DATE = 1200;
	public static final int CH_ID_LAST_RELOAD_DENOM = 1300;
	public static final int CH_ID_LAST_RELOAD_CHANNEL = 1400;
	public static final int CH_ID_LAST_SN_CHANNEL = 1500;
	public static final int CH_ID_CURRENT_BALANCE = 1600;
	public static final int CH_ID_PENDING_QUEUE_RPL = 1700;
	public static final int CH_ID_DUKCAPIL_STATUS = 1800;
	public static final int CH_ID_PAYMENT_CATEGORY = 1900;
	public static final int CH_ID_FULFILLMENT_KPI = 2000;
	public static final int CH_ID_SPWL_MSISDN_A = 2100;
	public static final int CH_ID_IS_HIBRID = 2200;
	public static final String SUCCESS = "Success";
	public static final String NOTFOUND = "Not Found";
	public static final String OK = "OK";
	public static final String NOT_OK = "Not OK";
	public static final String ESCAPE_NEWLINE = "$;$";
	public static final String REGISTRATION = "Registration";
	public static final String UNREGISTRATION = "Unregistration";
	public static final String UNREG = "unreg";
	public static final String NO_COST = "nocost";
	public static final String RECURRING = "Recurring";
	public static final String OPPURCHASE = "opPurchase";
	public static final String OPBATCHFULFILLMENT = "opBatchFulfillment";
	public static final String RECURRING_MANAGER = "RecurringManager";
	public static final String OPUNSUBSCRIBE = "opUnsubscribe";
	public static final String SGSN_ADDRESS_GPRS_WORDING = "GPRS Service have Update Location to HLR";
	public static final String NOT_SGSN_ADDRESS_GPRS_WORDING = "GPRS Service have not Update Location to HLR";
	public static final String AVAILABLE = "Available";
	public static final String NOT_AVAILABLE = "Not Available";
	public static final String ENABLE = "Enable";
	public static final String DISABLE = "Disable";
	public static final String PERMANENT = "Permanent";
	public static final String TEMPORARY = "Temporary";
	public static final String TRUE_SOCLIP = "WITH OVERRIDE CATEGORY";
	public static final String FALSE_SOCLIP = "WITHOUT OVERRIDE CATEGORY";
	public static final String RSA2 = "Roaming AKTIF untuk:<br><br><b>Roaming Partner Worry Free :</b><br>Layanan AKTIF untuk Data, Voice, SMS<br><br><b>Roaming Partner Promo :</b><br>Layanan AKTIF untuk Data, SMS. Layanan VOICE DIBLOK untuk roaming partner yang tidak ada kerjasama Camel (Offline Charging)<br><br><b>Roaming Partner Non-Promo :</b><br>Tidak bisa attach";
	public static final String RSA12 = "Roaming AKTIF untuk:<br><br><b>Roaming Partner Worry Free :</b><br>Layanan AKTIF untuk Data, Voice, SMS<br><br><b>Roaming Partner Promo :</b><br>Layanan AKTIF untuk Data, Voice( tidak online charging) , SMS. Please be aware of bill shock<br><br><b>Roaming Partner Non-Promo :</b><br>Tidak bisa attach";
	public static final String RSA13 = "Roaming AKTIF untuk:<br><br><b>Roaming Partner Worry Free :</b><br>Layanan AKTIF untuk Data, Voice, SMS<br><br><b>Roaming Partner Promo :</b><br>Layanan AKTIF untuk Data, Voice( tidak online charging) , SMS. Please be aware of bill shock<br><br><b>Roaming Partner Non-Promo :</b><br>Layanan AKTIF untuk Data, Voice, SMS (semua tidak online charging). Please be aware of bill shock";
	public static final String ACTIVE_IN_HLR = "Active In HLR";
	public static final String NOT_ACTIVE_IN_HLR = "Not Active In HLR";
	public static final String INTERNATIONAL_ROAMING_ACTIVE = "International Roaming Active";
	public static final String INTERNATIONAL_ROAMING_NOT_ACTIVE = "International Roaming Not Active";
	public static final String OUTGOING_SMS_OK = "OK (Parameter OBROGC = NO in SmartClient)";
	public static final String OUTGOING_SMS_NOT_OK = "NOT OK?(Parameter OBROGC = YES in SmartClient)";
	public static final String INCOMING_SMS_OK = "OK (Parameter OBRIGC = NO in SmartClient)";
	public static final String INCOMOING_SMS_NOT_OK = "NOT OK?(Parameter OBRIGC = YES in SmartClient)";
	public static final String APN_65534 = "APN_65534";
	public static final String INTERNET = "INTERNET";
	public static final String XLMMS = "WWW.XLMMS.NET";
	public static final String AXISMMS = "AXISMMS";
	public static final String BLACKBERRY = "BLACKBERRY";
}
