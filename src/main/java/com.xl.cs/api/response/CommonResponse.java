package com.xl.cs.api.response;

import java.io.Serializable;

/**
 * @author RONALN
 *
 */
public class CommonResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5990730983061502637L;

	private String responsecode;

	private String responsemessage;

	public CommonResponse(String responsecode, String responsemessage) {
		this.responsecode = responsecode;
		this.responsemessage = responsemessage;
	}

	public CommonResponse() {

	}

	public String getResponsecode() {
		return responsecode;
	}

	public void setResponsecode(String responsecode) {
		this.responsecode = responsecode;
	}

	public String getResponsemessage() {
		return responsemessage;
	}

	public void setResponsemessage(String responsemessage) {
		this.responsemessage = responsemessage;
	}

}
