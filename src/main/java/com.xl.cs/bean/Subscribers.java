package com.xl.cs.bean;

import java.util.List;

import com.xl.cs.entity.Candidate;

public class Subscribers {
	List<Candidate> subscribers;

	public List<Candidate> getSubscribers() {
		return subscribers;
	}

	public void setSubscribers(List<Candidate> subscribers) {
		this.subscribers = subscribers;
	}
	
}
