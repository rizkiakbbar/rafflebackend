package com.xl.cs.soa.response;

import java.io.Serializable;

public class CommonResponse implements Serializable {
	private String responsecode;

	private String responsemessage;

	public CommonResponse(String responsecode, String responsemessage) {
		this.responsecode = responsecode;
		this.responsemessage = responsemessage;
	}

	public CommonResponse() {

	}

	public String getResponsecode() {
		return responsecode;
	}

	public void setResponsecode(String responsecode) {
		this.responsecode = responsecode;
	}

	public String getResponsemessage() {
		return responsemessage;
	}

	public void setResponsemessage(String responsemessage) {
		this.responsemessage = responsemessage;
	}

}
