package com.xl.cs.service;

import java.util.List;

import com.xl.cs.entity.Candidate;

public interface CandidateService extends GenericService<Candidate, Long> {

	List<Candidate> getCandidateByStatus(String status);
	
	List<Candidate> getCandidateByStatus(String status,String batch);
	
	Candidate getCandidateByCoupon(String coupoun);
	
	List<Candidate> getShuffledCandidate();
	
	List<Candidate> getShuffledCandidate(String batch);

}
